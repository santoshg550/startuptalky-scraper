# Generated by Django 3.0.7 on 2020-06-30 18:55

from django.db import migrations, models
import django_mysql.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Headers',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('site', models.URLField(max_length=2000)),
                ('h2s', django_mysql.models.JSONField(default=dict)),
            ],
        ),
    ]
