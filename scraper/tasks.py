from celery.task.schedules import crontab
from celery.task import periodic_task
from celery import shared_task

from .scraper import scrape_all, scrape_one


@periodic_task(
    run_every=(crontab(minute=0, hour=0)),
    name='scrape_headers',
    ignore_result=True
)
def scrape_periodically():
    scrape_all()


@shared_task
def scrape_now(site):
    scrape_one(site)

    return f'{site} scraped successfully'
