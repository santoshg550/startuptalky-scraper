from django import forms


class NewSite(forms.Form):
    site = forms.URLField(max_length=2000)
