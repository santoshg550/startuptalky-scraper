from django.db import models

# JSONField is built-in starting with Django 3.1
from django_mysql.models import JSONField


class Headers(models.Model):
    site = models.URLField(max_length=2000)
    h2s = JSONField()
    date_scraped = models.DateTimeField(auto_now_add=True)
