from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('scrape/', views.scrape, name='scrape'),
    path('scraped/', views.SitesListView.as_view(), name='scraped'),
    path('results/', views.HeadersListView.as_view(), name='results')
]
