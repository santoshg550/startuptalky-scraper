from django.test import SimpleTestCase, TestCase
from django.urls import reverse
from django.db.models.query import QuerySet

from .scraper import scrape_one


class ScraperModuleTests(SimpleTestCase):
    pass


class ScraperScrapeTests(TestCase):
    def setUp(self):
        self.site = 'https://inc42.com/'

    def test_scrape_one(self):
        res = self.client.post(
            reverse('scrape'),
            { 'site': self.site }
        )

        self.assertEqual(res.status_code, 302)

        res = self.client.post(
            reverse('scrape')
        )

        self.assertEqual(res.status_code, 302)


class ScraperListSitesTests(TestCase):
    def setUp(self):
        self.site = 'https://inc42.com/'

    def test_sites_view(self):
        res = self.client.get(
            reverse('scraped')
        )

        self.assertEqual(res.status_code, 200)
        self.assertIsInstance(res.context['sites'], QuerySet)


# For faster test execution:
#
# python manage.py test scraper.tests.ScraperTestsWithDB --keepdb
