import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'startuptalky_scraper.settings')

app = Celery('startuptalky_scraper')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks()
